# DevOps Interview

### Part one - exercise 

Using Terraform output the list of cidrs from json response https://ip-ranges.atlassian.com/. Do not output IPv6 addresses.


### Part Two - questions

1. When would you use Google Kubernetes Engine managed Kubernetes instead of having full control with a full deployment using Kops/Rancher etc?
2. What would you do to a Kubernetes cluster to run production workloads?
3. Explain the difference between an ingress controller and a service, when would you use either?
4. How would you globally serve internet traffic to an API deployed in three regions using same domain name?
5. What is a shared VPC and how does it compare to vpc peering?
6. How do you ensure the state files in Terraform are lockable so are not overwritten by a work colleague?
7. When would you use Ansible/Salt instead of terraform?
8. How would you do traffic splitting to A/B version of a service in Kubernetes?
9. How would you allow multiple pods to write to the same volume mount in K8s?
10. How do you ensure a rolling deployment is successful in Kubernetes?

